<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class EntradaSalida extends Model
{
    protected $table = "entradasalida";
    protected $primaryKey ="id";
    protected $fillable = ["id","hora_entrada","hora_salida","detalle","id_persona"];

    public function persona()
    {
        return $this->belongsTo('App\Http\Models\Persona','id_persona','id');
    }

}
