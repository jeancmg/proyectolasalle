<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class Catalogo extends Model
{
    protected $table = "catalogo";
    protected $primaryKey ="id";
    protected $fillable = ["id","tipo","dato"];



}
