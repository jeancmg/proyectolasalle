<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller ;
use Illuminate\Http\Request;
use App\Company;
use App\Enum\RoleEnum;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\Model;

class BaseController extends Controller
{
    protected $sort = 'created_at';
    protected $sortDirection = 'asc';
    protected $limit = 10;
    protected $page = 0;
    protected $defaultSort = 'id';
    protected $permisibleOrders = ['created_at','name','price','category_id','stock','usage','trayectoria'];

    public function getPaginationParameters(Request $request)
    {
        if($request->has('sort')){
            $request->sort =  strtolower($request->sort);
            if(in_array($request->sort, $this->permisibleOrders) || $request->sort==$this->defaultSort)
                $this->sort= $request->sort;
        }

        if($request->has('sort_direction')){
            $request->sort_direction =  strtolower($request->sort_direction);
            if(in_array($request->sort_direction, ['asc','desc']))
                $this->sortDirection = $request->sort_direction;
        }

        if($request->has('limit'))
            $this->limit = $request->limit;

        if($request->has('page'))
            $this->page = $request->page;        
    }

    public function boolean($value){
        return (boolean)json_decode(strtolower($value));
    }

    public function error($message){
        return response()->json(["message"=>$message], 500);
    }

    public function notFound($message){
        return response()->json(["message"=>$message], 404);
    }

    public function hasAccessToCompany($idCompany){
        if(in_array(RoleEnum::Superadministrator, JWTAuth::getPayload()->get('authorities') )){
            if($idCompany == 0)
                return true;
            else {
                return $this->existInDB($idCompany, new Company());
            }
        }
        
        return $this->getCurrentCompanyId() == $idCompany;
    }

    public function getCurrentCompanyId(){
        return JWTAuth::parseToken()->authenticate()->company_id;
    }
    
    public function getCurrentUserId(){
        return JWTAuth::parseToken()->authenticate()->id;
    }

    public function existInDB($id, Model $class){
        $temp = $class::find($id);
        return !!isset($temp);
    }
}