<?php


namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Models\Catalogo;
use Illuminate\Http\Request;
use Exception;
use Symfony\Component\HttpFoundation\Response;


class CatalogoController
{
    public function  __construct()
    {
    }

    public  function index()
    {
        try{
            $listado = Catalogo::all();
            return response()->json($listado,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al listar catalogo ". $ex -> getMessage()],206);
        }

    }

    public function  show(Request $request,$id)
    {
        try{
            $catalogo = Catalogo::find($id);
            return response()->json($catalogo,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al encontrar el  id catalogo " . $id .":". $ex-> getMessage()],404);
        }

    }

    public function  create(Request $request)
    {
        try{
            $catalogo = Catalogo::create($request -> all());
            return response()->json($catalogo,Response::HTTP_CREATED);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al crear un catalogo ". $ex-> getMessage()],400);
        }

    }

    public function  update(Request $request,$id)
    {
        try{
            $catalogo = Catalogo::findOrfail($id);
            $catalogo -> update($request->all());
            return response()->json($catalogo,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al registrar en el catalogo".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function  delete(Request $request,$id)
    {
        try{
            Catalogo::find($id) -> delete();
            return response()->json([],Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error a eliminar lista en el catalogo".  $id .":". $ex-> getMessage()],400);
        }

    }

}
