<?php


namespace App\Http\Controllers;
use App\Http\Models\EntradaSalida;
use App\Http\Models\PersonaCarrera;
use Laravel\Lumen\Application;
use App\Http\Models\Persona;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;
class PersonaController extends BaseController
{
    public function  __construct()
    {
    }

    public function index(Application $app, Request $request){

        $this->getPaginationParameters($request);
        $query = Persona::orderBy($this->sort , $this->sortDirection);
        $query ->with(['tipo_persona','tipo_doc','tipo_idcarrera' => function($query){
            $query->with(['tipo_percarrera' => function($query){
                $query->with(['departamento']);
            }]);
        }]);
        if($request->has('tipo_persona')){
            if($request->tipo_persona == 7){//solo tipo 4 visitante
                $query->where('tipopersona_id',4);
                if($request->tipo_visitante == 1){//visitante que aun no sale
                    $query->join('entradasalida', 'persona.id', '=', 'entradasalida.id_persona');
                    $query->whereNull('entradasalida.hora_salida');
                }else{//visitante que ya salio
                    $query->join('entradasalida', 'persona.id', '=', 'entradasalida.id_persona');
                    $query->whereNotNull('entradasalida.hora_salida');
                }
            }else if($request->tipo_persona  == 0)// todos los tipos
                $query->where('tipopersona_id','<>', $request->tipo_persona);
            else if($request->tipo_persona )// tipo especifico
                $query->where('tipopersona_id', $request->tipo_persona);
        }
        if($request->has('nombre'))
            $query->where('nombre','LiKE','%'.$request->nombre.'%');
        if($request->has('numero')){
            $query->where('numero',$request->numero);
            $query->orWhere('sunedu',$request->numero);
        }
        if($request->has('busquedacomplete')){
            $query->where('numero','LiKE','%'.$request->busquedacomplete.'%');
            $query->orWhere('sunedu','LiKE','%'.$request->busquedacomplete.'%');
            $query->orWhere('nombre','LiKE','%'.$request->busquedacomplete.'%');
            $query->orWhere('apellidos','LiKE','%'.$request->busquedacomplete.'%');
        }
        return new JsonResponse($query->paginate($this->limit));

    }


    public function  show(Request $request,$id)
    {
        try{
            $persona = Persona::with(['tipo_persona','tipo_doc','tipo_idcarrera' => function($query){
                $query->with(['tipo_percarrera' => function($query){
                    $query->with(['departamento']);
                }]);
            }])->find($id);
            return response()->json($persona,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al encontrar el cargo con persona" . $id .":". $ex-> getMessage()],404);
        }

    }

    public function  create(Request $request)
    {
        try{
            $carreras = $request->tipo_idcarrera;
            $persona = Persona::create($request -> all());
            for ($i=0;$i < sizeof($carreras); $i++){
                $carrera = new PersonaCarrera;
                $carrera->id_persona = $persona['id'];
                $carrera->id_carrera = $carreras[$i]['id_carrera'];
                $carrera->save();
            }
            if($request->tipopersona_id == 4){
                $entrada_salida = new EntradaSalida();
                $entrada_salida->hora_entrada = $request->entrada_salida['entrada'];
                $entrada_salida->detalle = $request->entrada_salida['detalle'];
                $entrada_salida->id_persona = $persona['id'];
                $entrada_salida->save();
            };
            return response()->json($persona,Response::HTTP_CREATED);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al registrar un persona". $ex-> getMessage()],400);
        }
    }

    public function  update(Request $request,$id)
    {
        try{
            $carreras = $request->tipo_idcarrera;
            $persona = Persona::findOrfail($id);
            $persona->tipo_idcarrera()->delete();
            $persona -> update($request->all());
            for ($i=0;$i < sizeof($carreras); $i++){
                $carrera = new PersonaCarrera;
                $carrera->id_persona = $persona['id'];
                $carrera->id_carrera = $carreras[$i]['id_carrera'];
                $carrera->save();
            }
            return response()->json($persona,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al actualizar un persona".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function finalizar_entrada(Request $request, $id)
    {
        try{
            $entrada_salida = EntradaSalida::whereNull('hora_salida')->findOrfail($id);
            $entrada_salida->hora_salida = $request->salida;
            $entrada_salida->save();
            return response()->json($entrada_salida,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al finalizar registro de entrada de persona ".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function iniciar_entrada(Request $request, $id)//id entrada detalle
    {
        try{
            // $entrada_salida = EntradaSalida::whereNull('hora_salida')->findOrfail($id);
            $entrada_salida = new EntradaSalida;
            $entrada_salida->id_persona = $id;
            $entrada_salida->hora_entrada = $request->entrada;
            $entrada_salida->detalle = $request->detalle;
            $entrada_salida->save();
            return response()->json($entrada_salida,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al crear registro de entrada de persona ".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function  delete(Request $request,$id)
    {
        try{
//            Persona::find($id) -> delete();
            $persona = Persona::findOrFail($id);

            DB::beginTransaction();
            $persona->tipo_idcarrera()->delete();
            $persona->delete();
            DB::commit();
            return response()->json([],Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error a eliminar el usuario con id".  $id .":". $ex-> getMessage()],400);
        }

    }





}

