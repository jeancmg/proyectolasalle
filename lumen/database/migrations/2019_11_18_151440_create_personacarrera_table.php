<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonacarreraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personacarrera', function (Blueprint $table) {
            $table->increments('id');



            $table->integer('id_persona');
            $table->foreign('id_persona')->references('id')-> on('persona')-> onDelete('cascade')-> onUpdate('cascade');
            $table->integer('id_carrera');
            $table->foreign('id_carrera')->references('id')-> on('carrera')-> onDelete('cascade')-> onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personacarrera');
    }
}
