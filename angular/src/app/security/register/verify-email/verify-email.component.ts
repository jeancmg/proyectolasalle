import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {LinksService} from '../../../api/links.service';
import {IMessage} from '../../../api/Model';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit, OnDestroy {

  public sub: Subscription;
  public massageActivate: IMessage;
  public MessageType ='';
  public loader: boolean;
  dialogRef: any;
  constructor(private route: ActivatedRoute,
              public dialog: MatDialog,
              private router: Router,
              private linkService: LinksService) {
    this.loader = true;
  }

  dialogs(responseService: string, message: string, header: string, error: any[], errorsmessage: string){

    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        errormessage: errorsmessage
      }});
  }

  ngOnInit() {
      this.sub =  this.route.queryParams.subscribe(
        params => {
          if (params.codigo){

              this.MessageType = 'Activando';
              this.linkService.validarCorreo(params.codigo).subscribe(
                data => {
                  this.loader = false;
                  this.massageActivate = data;
                  this.dialogs('alert-succesful', this.massageActivate.message , '¡Éxito!', null, '');
                  this.router.navigate( ['/login']);
                },
                error => {

                  this.loader = false;
                  this.dialogs('alert-error', 'Ocurrió algún error en la activación de su cuenta.', 'Upss..', error.error.subErrors, error.error.message);
                  this.MessageType = 'Enviado';
                }
              );
          } else {
            this.MessageType = 'Enviado';
          }
        }
      );
  }



  reenviar(){
  const der = localStorage.getItem('username_register');
    // console.log(der);
  this.linkService.reenviarCorreoValidacion(der).subscribe(
    data => {

      this.dialogs('alert-succesful', data.message , '¡Éxito!', null, '');
    },
    error1 => {
      // console.log(error1);
      this.dialogs('alert-error', 'Ocurrió algún error en el reenvio de correo de activación de su cuenta.', 'Upss..', error1.error.subErrors, error1.error.message);
    }
  );
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}
