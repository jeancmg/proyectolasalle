import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatPaginatorModule, MatProgressBarModule,
  MatSelectModule, MatSidenavModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule, MatTabsModule, MatTooltipModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {BuscadorModule} from '../main/content/buscador/buscador.module';
// import {ImgResponsiveModule} from '../main/general/componentes/img-responsive/img-responsive.module';
// import {ImgUploadModule} from '../main/general/componentes/img-upload/img-upload.module';
import {LoaderModule} from '../../general/directivas/loader/loader.module';
import {AlertDialogModule} from '../../general/componentes/alert-dialog/alert-dialog.module';
// import {PaginatorModule} from '../main/general/componentes/paginator/paginator.module';
import {LoginModule} from '../login/login.module';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FuseModule} from '../../../@fuse/fuse.module';
import { RegisterUserComponent } from './register-user/register-user.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatSortModule,
    MatStepperModule,
    MatChipsModule,
    MatTabsModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    // BuscadorModule,
    // ImgResponsiveModule,
    // ImgUploadModule,
    LoaderModule,
    AlertDialogModule,
    // PaginatorModule,
    MatPaginatorModule,
    MatProgressBarModule,
    LoginModule,
    MatSidenavModule,
    RouterModule,
    MatAutocompleteModule,
    FuseSharedModule,
    FuseModule,
    BrowserAnimationsModule
  ],
  declarations: [RegisterComponent, RegisterUserComponent, VerifyEmailComponent]
})
export class RegisterModule { }
