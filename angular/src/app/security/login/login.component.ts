import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AuthenticationService} from './services/authentication.service';
import {FuseConfigService} from '../../../@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import {environment} from '../../../environments/environment';

import {DomSanitizer} from '@angular/platform-browser';

import {AlertDialogComponent} from '../../general/componentes/alert-dialog/alert-dialog.component';
import {MatDialog} from '@angular/material';


@Component({templateUrl: 'login.component.html', styleUrls  : ['./login.component.scss'],
  animations : fuseAnimations})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  submitted = false;

  errorsmap: string;
  errorsdescription: string;
  recuerdame = false;
  loading = false;
  error = '';
  loginFormErrors: any;
  parent;
  dialogRef: any;
  dialogRefAlert: any;

  onLoginFormValuesChanged()
  {
    for ( const field in this.loginFormErrors )
    {
      if ( !this.loginFormErrors.hasOwnProperty(field) )
      {
        continue;
      }

      // Clear previous errors
      this.loginFormErrors[field] = {};

      // Get the control
      const control = this.loginForm.get(field);

      if ( control && control.dirty && !control.valid )
      {
        this.loginFormErrors[field] = control.errors;
      }
    }
  }
  constructor(
              public dialog: MatDialog,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private fuseConfig: FuseConfigService,
              private sanitizer: DomSanitizer,
              private authenticationService: AuthenticationService) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });
    this.loginFormErrors = {
      email: {},
      password: {}
    };
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email   : ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      recuerdame: [false, []]
    });

    // reset login status
    this.authenticationService.logout(true);
    this.loginForm.valueChanges.subscribe(() => {
      this.onLoginFormValuesChanged();
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    console.log('submit');
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.iniciarSesion(this.f.email.value, this.f.password.value)
    // . pipe(first())
      .subscribe(
        data => {
          console.log('ok');
          this.sessionExitosa(data);
        },
        error => {
          console.log('fallo');
          this.dialogs('alert-error', 'Verifique su usuario y contraseña', 'Upss..', null, 'error');
          this.loading = false;
        });
  }



  sessionExitosa(data){
    this.authenticationService.cargarUsuario()
      .pipe(first())
      .subscribe(
        data => {
          let returnUrl =  '/app/personas';
          const params = {};
          for (const key in this.route.snapshot.queryParams){
            if (key == 'returnUrl'){
              returnUrl = this.route.snapshot.queryParams[key];
            } else {
              params[key] = this.route.snapshot.queryParams[key];
            }
          }
          this.router.navigate([returnUrl], {queryParams: params} );
          this.loading = false;
        },
        error => {
          this.error = error;
          this.loading = false;
        }
      );
  }


  dialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string){
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }});
  }
}
