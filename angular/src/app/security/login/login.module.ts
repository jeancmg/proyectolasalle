import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule, MatCheckboxModule, MatChipsModule, MatDialog, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
  MatRippleModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { LoginComponent} from './login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FuseDirectivesModule} from '../../../@fuse/directives/directives';
import {FusePipesModule} from '../../../@fuse/pipes/pipes.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LoaderModule} from '../../general/directivas/loader/loader.module';
import {HideUnauthorizedDirective} from './directives/hide-unauthorized.directive';
import {ShowAuthorizedDirective} from './directives/show-authorized.directive';
import {DisabledUnauthorizedDirective} from './directives/disabled-unauthorized.directive';
import {DialogLockedComponent} from './dialog-locked.component';
import {AlertDialogComponent} from '../../general/componentes/alert-dialog/alert-dialog.component';
import {AlertDialogModule} from '../../general/componentes/alert-dialog/alert-dialog.module';

@NgModule({
  declarations: [
    LoginComponent,
    HideUnauthorizedDirective,
    DisabledUnauthorizedDirective,
    ShowAuthorizedDirective,
    DialogLockedComponent
  ],
  imports     : [
    RouterModule,
    FuseSharedModule,
    FlexLayoutModule,
    FuseDirectivesModule,
    FusePipesModule,
    BrowserAnimationsModule,
    LoaderModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatCheckboxModule,
    MatChipsModule,
    AlertDialogModule,
    MatInputModule
  ],
  exports: [HideUnauthorizedDirective,
    ShowAuthorizedDirective,
    DisabledUnauthorizedDirective],
  entryComponents: [
    DialogLockedComponent,
    AlertDialogComponent
  ]
})
export class LoginModule
{
}
