import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {LoginService} from '../../../api/login.service';
import {IToken, IUser} from '../../../api/Model';
import {Rol, Role} from '../models/roles';
// tslint:disable-next-line:import-blacklist
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {UserService} from '../../../api/user.service';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  public observable: BehaviorSubject<boolean>;

  constructor(private loginService: LoginService,
              private userService: UserService) {
    this.observable = new BehaviorSubject<boolean>(this.isLogin());
  }

  iniciarSesion(usuario: string, contraseña: string) {
    return this.loginService.iniciarSesion(usuario, contraseña)
      .pipe(map((res: IToken) => {
        console.log(res);
        this.inicioExitoso(res);
      }));
  }

  refrescarSesion(): Observable<any> {
    return this.loginService.refrescarSesion(this.getToken().token)
      .pipe(map((res: IToken) => {
        // login successful if there's a jwt token in the response
        if (res && res.token) {
          // decodedJwtData.authorities;
          // store username and jwt token in local storage to keep user logged in between page refreshes
          const recuerdame: boolean = this.isRememberme();
          if (recuerdame) {
            localStorage.setItem('token', JSON.stringify(res));
          } else {
            sessionStorage.setItem('token', JSON.stringify(res));
          }

          this.observable.next(true);
        }
      }));
  }

  cargarUsuario() {
    return this.userService.miUsuario().pipe(map((user: IUser) => {
      // if (user && user.name) {
          localStorage.setItem('current_user', JSON.stringify(user));
          sessionStorage.setItem('current_user', JSON.stringify(user));
      // }
    }));
  }

  logout(noBorrar: boolean) {
    // remove user from local storage to log user out
    localStorage.removeItem('rememberme');
    localStorage.removeItem('token');
    localStorage.removeItem('current_user');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('current_user');
    if (!noBorrar) {
      this.observable.next(false);
    }
  }

  getToken(): IToken {
    let token: string;
    token = localStorage.getItem('token');
    token = sessionStorage.getItem('token');
    return JSON.parse(token) as IToken;
  }

  getUser(): any {
    const recuerdame: boolean = this.isRememberme();
    let user: string;
    if (recuerdame) {
      user = localStorage.getItem('current_user');
    } else {
      user = sessionStorage.getItem('current_user');
    }
    return JSON.parse(user) as any;
  }

  updateUser(user: IUser) {
    localStorage.removeItem('current_user');
    const recuerdame: boolean = this.isRememberme();
    if (recuerdame) {
      localStorage.setItem('current_user', JSON.stringify(user));
    } else {
      sessionStorage.setItem('current_user', JSON.stringify(user));
    }
  }

  getRoles(): Role[] {
    const currentUser = this.getToken();
    const roles: Role[] = [];
    if (currentUser && currentUser.token) {
      const jwtData = currentUser.token.split('.')[1];
      const decodedJwtJsonData = window.atob(jwtData);
      const decodedJwtData = JSON.parse(decodedJwtJsonData);

      for (const item in decodedJwtData.authorities) {
        roles.push(Rol.fromInt(decodedJwtData.authorities[item]));
      }
    }
    return roles;

  }


  hasRolPermision(roles: Role[]): boolean {
    if (roles.length == 0) {
      return true;
    }
    const rolesSaved = this.getRoles();
    for (const rolRequest in roles) {
      for (const rolSaved in rolesSaved) {
        if (roles[rolRequest] == rolesSaved[rolSaved]) {
          return true;
        }
      }
    }
    return false;
  }

  isLogin(): boolean {
    return !!this.getToken();
  }

  isRememberme(): boolean {
    return localStorage.getItem('rememberme') === 'true';
  }

  private inicioExitoso(res: IToken) {
    if (res && (res.token)) {
      localStorage.setItem('token', JSON.stringify(res));
      sessionStorage.setItem('token', JSON.stringify(res));
      this.observable.next(true);
    }
  }

}
