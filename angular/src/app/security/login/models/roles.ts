export enum Role {
  Superadministrador = 1,
  ScentinelaMaster = 2,
  Scentinela = 3,
  Usuer = 4
}

export namespace Rol {
  export function fromInt(s: number): Role{
    let  r: Role = null;
    for (const item in Role) {
      if (Role[item.toString()] == s) {
        r = Role[item.toString()] as Role;
        break;
      }
    }
    return r;
  }


  export function getName(role: Role): string{
    let name = '';
    switch (role) {
      case Role.Superadministrador:
        name = 'Superadministrador';
        break;
      case Role.ScentinelaMaster:
        name = 'Scentinela Master';
        break;
      case Role.Scentinela:
        name = 'Scentinela';
        break;
      case Role.Usuer:
        name = 'Usuario';
        break;
      default:
        break;
    }
    return name;
  }
}

