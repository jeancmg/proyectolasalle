import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatchPassword} from '../../../general/validators/matchPass';
import {IRole} from '../../../api/Model';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../api/user.service';
import {FuseConfigService} from '../../../../@fuse/services/config.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthenticationService} from '../../login/services/authentication.service';
import {HttpClient} from '@angular/common/http';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';
import {LinksService} from '../../../api/links.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'fuse-app-recovery-pass',
  templateUrl: './recovery-pass.component.html',
  styleUrls: ['./recovery-pass.component.scss']
})
export class RecoveryPassComponent implements OnInit {

  public newPassFormGroup: FormGroup;
  public loader: boolean;
  public initial: boolean;
  public codeError: boolean;
  public message: string;
  public colorPassword = 'warn';
  public valuePassword = 0;
  public passwordMessage = '';
  dialogRef: any;
  codigo: string;
  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fuseConfig: FuseConfigService,
              private sanitizer: DomSanitizer,
              private authenticationService: AuthenticationService,
              private httpClient: HttpClient,
              private linkService: LinksService) { }

  ngOnInit() {
    this.initial = false;
    this.codeError = false;
    this.message = 'Validando enlace';
    this.initnewPassFormGroup();
    this.route.queryParams.subscribe(
      params => {
        this.codigo =  params.token;
        this.linkService.validarSolicitudPass(this.codigo).subscribe( response => {
            this.initial = true;
          }, error => {
            this.initial = false;
            this.codeError = true;
            this.message = error.error.message;
            this.dialogs('alert-error', 'Algo anda mal con el enlace' , 'Upss..', [], error.error.message);
          }
          );
      }
    );
  }

  initnewPassFormGroup(){
    this.newPassFormGroup = new FormGroup({
      'password':            new FormControl('', [Validators.required, Validators.minLength(6), MatchPassword()]),
      'passwordConfirm':            new FormControl('', [Validators.required, Validators.minLength(6),  MatchPassword()]),
    });
  }

  onKey() {
    this.validarContraseña();
  }

  validarContraseña() {
    this.valuePassword = 0;
    const expRegLetrasMin = new RegExp('[a-z]+');
    const expRegLetrasMax = new RegExp('[A-Z]+');
    const expRegLetrasSim = new RegExp('[^a-zA-Z0-9]+');
    const expRegNumeros = new RegExp('[0-9]+');
    const tamanio = this.newPassFormGroup.get('password').value.toString().length;
    if (expRegLetrasMin.test(this.newPassFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegLetrasMax.test(this.newPassFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegLetrasSim.test(this.newPassFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegNumeros.test(this.newPassFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (tamanio >= 8) {
      this.valuePassword = this.valuePassword + 20;

    } else {
      this.valuePassword = 0;
    }
    switch (this.valuePassword) {
      case 0: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Débil';
        break;
      }
      case 20: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Débil';
        break;
      }
      case 40: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Insegura';
        break;
      }
      case 60: {
        this.colorPassword = 'primary';
        this.passwordMessage = 'Media';
        break;
      }
      case 80: {
        this.colorPassword = 'primary';
        this.passwordMessage = 'Seguro';
        break;
      }
      case 100: {
        this.colorPassword = 'accent';
        this.passwordMessage = 'Seguro';
        break;
      }
    }
  }

  dialogs(responseService: string, message: string, header: string, error: any[], errormessage: string){

    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        errormessage: errormessage
      }});
  }

  recoverypass(){
    if(this.loader) return;
    this.loader= true;
    let password: string;
    let matchpassword: string;

    password = this.newPassFormGroup.get('password').value;
    matchpassword = this.newPassFormGroup.get('passwordConfirm').value;

    this.linkService.newPass(this.codigo, password).subscribe(
      data => {
        if (data){
          this.loader = false;

          this.dialogs('alert-succesful', 'La recuperación de tu contraseña fue realizada con éxito ', '¡Éxito!', null, data.message);
          this.router.navigate( ['/login']);
        }
      },
      error => {
        // console.log(error.error.subErrors);
        this.loader = false;
        this.dialogs('alert-error', 'Ocurrió algún error mientras se recuperaba su contraseña.', 'Upss..', error.error.subErrors, error.error.message);

      }
    );

  }

}
