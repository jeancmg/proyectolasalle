import { Component, OnInit } from '@angular/core';
import {fuseAnimations} from '../../../@fuse/animations';

@Component({
  selector: 'fuse-app-resetpass',
  templateUrl: './resetpass.component.html',
  styleUrls: ['./resetpass.component.scss'],
  animations : fuseAnimations
})
export class ResetpassComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
