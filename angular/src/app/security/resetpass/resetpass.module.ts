import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetpassComponent } from './resetpass.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { VerifyPassEmaillComponent } from './verify-pass-emaill/verify-pass-emaill.component';
import { RecoveryPassComponent } from './recovery-pass/recovery-pass.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatPaginatorModule, MatProgressBarModule,
  MatSelectModule, MatSidenavModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule, MatTabsModule, MatTooltipModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoaderModule} from '../../general/directivas/loader/loader.module';
import {AlertDialogModule} from '../../general/componentes/alert-dialog/alert-dialog.module';
import {LoginModule} from '../login/login.module';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {FuseModule} from '../../../@fuse/fuse.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatSortModule,
    MatStepperModule,
    MatChipsModule,
    MatTabsModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    LoaderModule,
    AlertDialogModule,    
    MatPaginatorModule,
    MatProgressBarModule,
    LoginModule,
    MatSidenavModule,
    RouterModule,
    MatAutocompleteModule,
    FuseSharedModule,
    FuseModule,    BrowserAnimationsModule
  ],
  declarations: [ResetpassComponent, ConfirmEmailComponent, VerifyPassEmaillComponent, RecoveryPassComponent],
  exports: [
    ResetpassComponent, ConfirmEmailComponent, VerifyPassEmaillComponent, RecoveryPassComponent
  ]
})
export class ResetpassModule { }
