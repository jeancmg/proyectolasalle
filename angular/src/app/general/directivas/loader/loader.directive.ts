
import {Directive, ElementRef, Input, Renderer2} from "@angular/core";

@Directive({
  selector: '[loader]'
})
export class LoaderDirective {
  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  @Input() loadSize: string = "100px";

  @Input() set loader(loading: boolean) {
    let renderer = this.renderer;
    if(loading){
      this.renderer.addClass(this.el.nativeElement, 'loader');
      this.renderer.setStyle(this.el.nativeElement, 'width', this.loadSize);
      this.renderer.setStyle(this.el.nativeElement, 'height', this.loadSize);


      this.el.nativeElement.childNodes.forEach(function(t){
        if(t.nodeName!="#comment")
          renderer.setStyle(t, 'display', 'none');
      });

    } else {
      this.renderer.removeStyle(this.el.nativeElement, 'height');
      this.renderer.removeStyle(this.el.nativeElement, 'width');
      this.renderer.removeClass(this.el.nativeElement, 'loader');
      this.el.nativeElement.childNodes.forEach(function(t){
        if(t.nodeName!="#comment")
          renderer.removeStyle(t, 'display');

      });
    }
  }
}
