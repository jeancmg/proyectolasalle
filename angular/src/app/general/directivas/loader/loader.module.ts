import { NgModule } from '@angular/core';
import {LoaderDirective} from './loader.directive';


@NgModule({
  declarations: [
    LoaderDirective
  ],
  exports: [
    LoaderDirective,
  ],
  entryComponents: [
  ]
})
export class LoaderModule
{
}
