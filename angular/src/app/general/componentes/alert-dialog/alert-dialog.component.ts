import {Component, EventEmitter, Inject, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'fuse-app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AlertDialogComponent implements OnInit {
  @Output() responseAcceptReject = new EventEmitter<boolean>();
  public responseService: string;
  public message: string;
  public header: string;
  public errors: any[];
  public errormessage: string;
  public icon: string;
  public icon_color: string;
  private translate: any;
  constructor( public dialogRef: MatDialogRef<AlertDialogComponent>,
               @Inject(MAT_DIALOG_DATA) private data: any) {
    this.responseService = data.responseService;
    this.message = data.message;
    this.header = data.header;
    this.errors = data.error;
    this.errormessage = data.errormessage;
    this.icon = data.icon;
    this.icon_color = data.icon_color;
    this.translate = {
      'id': 'Identificador'
    };
  }

  ngOnInit() {

  }

  confirmacionAlerta(action: boolean){
      this.responseAcceptReject.emit(action);
      this.dialogRef.close();
  }

  getFiled(field: string): string{
    if (this.translate[field]) {
      return this.translate[field];
    }
    return field;
  }

}
