import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {PaginatorComponent} from './paginator.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatPaginator} from '@angular/material';


@NgModule({
  declarations: [
    PaginatorComponent
  ],
  exports: [
    PaginatorComponent,
  ],
  imports: [
    FlexLayoutModule,
    MatPaginatorModule
  ],
  entryComponents: [
    MatPaginator
  ]
})
export class PaginatorModule
{
}
