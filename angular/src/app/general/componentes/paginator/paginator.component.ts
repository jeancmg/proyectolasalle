import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Page} from './page';
import {IModel, IPageable} from '../../../api/Model';
import {PageEvent} from '@angular/material';
import { MatPaginatorIntl } from '@angular/material';
import {MyCustomPaginatorIntl} from './matpaginatorintl';

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  providers: [
    { provide: MatPaginatorIntl, useValue: new MyCustomPaginatorIntl() }
  ]

})
export class PaginatorComponent {
  myCustomPaginatorIntl: MyCustomPaginatorIntl;
  @Input('data') pageable: IPageable<IModel>;
  @Output() change: EventEmitter<Page>;

  constructor( private  matPaginatorIntl: MatPaginatorIntl){
    this.myCustomPaginatorIntl = <MyCustomPaginatorIntl>matPaginatorIntl;
    this.myCustomPaginatorIntl.changes.next();
    // console.log(this.pageable);
    this.change = new EventEmitter();

  }

  page(pageEvent: PageEvent){
    const page  = new Page();
    page.number = pageEvent.pageIndex;
    page.size = pageEvent.pageSize;

    this.change.emit(page);
  }

}
