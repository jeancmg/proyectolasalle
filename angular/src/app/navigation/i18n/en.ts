export const locale = {
    lang: 'en',
  data: {
    'NAV': {
      'INTRO': 'Introduction',
      'HOWTOUSE': 'How to Use',
      'CREDITS': 'Credits',
      'CONTACT': 'Contact',
      'TUTOS': 'Tutorials'
    }
  }
};
