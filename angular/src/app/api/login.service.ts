import { Injectable } from '@angular/core';
import { IToken, IUser} from './Model';
import {BehaviorSubject} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = environment.apiHost;

  constructor(private httpClient: HttpClient) { }

  private getAutorization(){
    let headers = new HttpHeaders();
    // let authorization: string = environment.jwt_client_id+":"+environment.jwt_client_secret;
    // headers = headers.append("Authorization", "Basic " + btoa(
    //   authorization));
    return headers;
  }
  iniciarSesion(usuario: string, contraseña: string): Observable<IToken> {
    let headers = this.getAutorization();

    const formdata: FormData = new FormData();
    formdata.append('email', usuario);
    formdata.append('password', contraseña);
    return this.httpClient.post<IToken>(this.url + '/login' , formdata , {
      headers
    });
  }

  refrescarSesion(refresh_token: string): Observable<IToken> {
    let headers = this.getAutorization();
    return this.httpClient.patch<IToken>(this.url + '/auth/refresh' ,
      {'grant_type': 'refresh_token',
        'token': refresh_token} , {
        headers
      });
  }

}
