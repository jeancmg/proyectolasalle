import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IMessage} from './Model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  private url = environment.apiHost + '/link';
  constructor(private httpClient: HttpClient) { }

  validarCorreo(activate_code: string): Observable<IMessage>{
    let params = new HttpParams();
    params = params.set('codigo', activate_code);
    return this.httpClient.post<IMessage>(this.url + '/validar-correo', {}, {params: params});
  }

  reenviarCorreoValidacion(username: any){
    return this.httpClient.post<IMessage>(this.url + '/reenviar-validacion', {'username': username }, {});
  }

  solicitarRecuperarPass(email: string): Observable<any>{
    return this.httpClient.post<any>(this.url + '/resset-pass', {'email': email }, {});
  }
  validarSolicitudPass(activate_code: string){
    let params = new HttpParams();
    params = params.set('token', activate_code);
    return this.httpClient.get<IMessage>(this.url + '/resset-pass', {params: params});
  }
  newPass(activate_code: string, pass: any){
    return this.httpClient.put<IMessage>(this.url + '/resset-pass', {'password': pass, 'token': activate_code});
  }

}
