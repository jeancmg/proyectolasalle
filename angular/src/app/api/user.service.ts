import { Injectable } from '@angular/core';
import {IPageable, IPageRequest, IPageRequestX, IUser} from './Model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Rx';
import {PaginationService} from './PaginationService';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = environment.apiHost ;

  constructor(private httpClient: HttpClient) { }

  miUsuario(): Observable<IUser>{
    return this.httpClient.get<IUser>(this.url + '/profile').pipe();
  }

  cambiarContrasena(id: string, pass: string, matchpass: string): Observable<IUser>{
    return this.httpClient.put<IUser>(this.url + '/mi-perfil/contrasena', {'id': id, 'password': pass, 'matchingPassword': matchpass }).pipe();
  }

  lista(page: IPageRequestX, nombre_usuario: string, apellidos: string): Observable<IPageable<IUser>>{
    let params = PaginationService.getPageParms(page);
    if (nombre_usuario == null) {
      params = params.set('nombre_usuario', '');
    } else {
      params = params.set('nombre-usuario', nombre_usuario);
    }
    if (apellidos == null) {
      params = params.set('apellidos', '');
    } else {
      params = params.set('apellidos', apellidos);
    }

    return this.httpClient.get<IPageable<IUser>>(this.url , {params}).pipe();
  }

  userRegister(registro: IUser){
    return this.httpClient.post<IUser>(this.url + '/register', registro);
  }

}
