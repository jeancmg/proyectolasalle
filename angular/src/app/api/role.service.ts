import { Injectable } from '@angular/core';
import { IRoleUser, IRole } from './Model';
import {BehaviorSubject} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Rx';
import {PaginationService} from './PaginationService';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private url = environment.apiHost ;

  constructor(private httpClient: HttpClient) { }

  misPermisos(): Observable<IRoleUser[]>{
    return this.httpClient.get<IRoleUser[]>(this.url + '/auth/roles').pipe();
  }
  
  roles(): Observable<IRole>{
    return this.httpClient.get<IRole>(this.url + '/roles').pipe();
  }

}
