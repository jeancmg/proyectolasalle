import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPageable, IPageRequestX, IUser} from '../Model';
import {AuthenticationService} from 'app/security/login/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = environment.apiHost ;
  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) { }

  listarUsuarios(type: string, page?: IPageRequestX): Observable<IPageable<IUser>> {
    let params = new HttpParams();
    // params = PaginationServiceX.getPageParms(page);
    let nulable = '';
    (type != null) ? params = params.set('type', type) : nulable = '';
    return this.httpClient.get<IPageable<IUser>>(this.url + '/users', {params}).pipe();
  }

  save (user: IUser): Observable<IUser>{
    let params = new HttpParams();
    return this.httpClient.post<IUser>(this.url + '/users', user, {params});
  }

  usuario(id: number): Observable<IUser>{
    let params = new HttpParams();
    return this.httpClient.get<IUser>(this.url + '/users/' + id + '', {params}).pipe();
  }

  edit(usuario: IUser, id: number) {
    let params = new HttpParams();
    return this.httpClient.put(this.url + '/users/' + id + '', usuario, {params});
  }

  delete(id: number) {
    let params = new HttpParams();
    return this.httpClient.delete(this.url + '/users/' + id + '', {params});
  }
}


