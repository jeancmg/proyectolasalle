import {Component, OnInit, ViewChild} from '@angular/core';
import {PersonaService} from '../../../../api/persona.service';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {CrearPersonaComponent} from '../../persona/crear-persona/crear-persona.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CrearEntradasSalidasComponent} from '../crear-entradas-salidas/crear-entradas-salidas.component';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';



@Component({
  selector: 'fuse-app-listar-entradas-salidas',
  templateUrl: './listar-entradas-salidas.component.html',
  styleUrls: ['./listar-entradas-salidas.component.scss']
})
export class ListarEntradasSalidasComponent implements OnInit {

  public listadata = null;
  public listadatacomplete: any;
  public displayedColumns: string[] = ['name', 'document', 'ingreso', 'salida', 'descripcion', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dialogRefAlert: any;
  nombrePersona = '';
  tipoIngresante = 1;
  constructor(private personaService: PersonaService , public dialog: MatDialog) {
    this.listadata = new MatTableDataSource([]);
    this.paginator = this.listadata.paginator;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CrearEntradasSalidasComponent , {
      width: '450px',
    });
    dialogRef.afterClosed().subscribe(
      data => {this.ngOnInit();
      });
  }

  ngOnInit() {
    this.cargarData(1, 10);
  }

  cargarData(page, size){
    this.personaService.listarPersonas((this.nombrePersona == '') ? null : this.nombrePersona , 7, null, this.tipoIngresante.toString(), {page: page, limit: size, sortDirection: null, sort: null}).subscribe(
      data => {
        if (data.data.length > 0) {
          this.listadata = new MatTableDataSource(data.data);
          this.listadatacomplete = data;
        } else {
          this.listadata = null;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  private onEnter(event: any)
  {
    if (event.keyCode == 13)
    {
      this.cargarData(0, 10);
    }
  }

  salida(id){
    this.openAlertdialogs('alert-confirmed', '¿Desea registrar la SALIDA de la persona?', '¡Alerta!', null, 'alert');
    this.dialogRefAlert.componentInstance.responseAcceptReject.subscribe((response: boolean) => {
      if (response === true) {
        this.personaService.salidaPersona(id).subscribe(
          data => {
            this.openAlertdialogs('alert-succesful', 'Se registro la SALIDA del visitante', '¡Éxito!', null, 'success');
            this.cargarData(1, 10);
          },
          error => {
            this.openAlertdialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
          }
        );
      }
    });
  }
}
