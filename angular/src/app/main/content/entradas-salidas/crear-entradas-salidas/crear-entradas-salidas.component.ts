import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonaService} from '../../../../api/persona.service';
import {MAT_DIALOG_DATA, MatDatepickerModule, MatDialog, MatDialogRef, MatNativeDateModule} from '@angular/material';
import {ICarrera, ICatalogo, IPersona, IPersonacarrera} from '../../../../api/Model';
import {BarecodeScannerLivestreamComponent} from 'ngx-barcode-scanner';
import {BehaviorSubject, Subscription} from 'rxjs';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

export interface Tipodoc {
  value: string;
  viewValue: string;
}

export interface Tipoper {
  value: string;
  viewValue: string;
}

export interface Tipocar {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'fuse-app-crear-entradas-salidas',
  templateUrl: './crear-entradas-salidas.component.html',
  styleUrls: ['./crear-entradas-salidas.component.scss']
})


export class CrearEntradasSalidasComponent implements OnInit, AfterViewInit {



  tipodoc: Tipodoc[] = [
    {value: '1', viewValue: 'Documento de Identidad'},
    {value: '2', viewValue: 'Carnet de Extranjeria'},
    {value: '3', viewValue: 'Pasaporte'}
  ];
  tipoper: Tipoper[] = [
    {value: '4', viewValue: 'Visitante'},
  ];

  tipocar: Tipocar[] = [

    {value: 1, viewValue: 'Administraciòn'},
    {value: 2, viewValue: 'Derecho'},
    {value: 3, viewValue: 'Ing de Sotfware'}
  ];




  @ViewChild(BarecodeScannerLivestreamComponent)
  barecodeScanner: BarecodeScannerLivestreamComponent;
  dialogRefAlert: any;
  barcodeValue;
  public personaFG: FormGroup;
  public entradaFG: FormGroup;
  tipocarnet: boolean;
  personaBuscada: any;
  nuevaPersona = 'SIN_BUSQUEDA';
  visita = 'NO_VISITA';
  constructor(private personaService: PersonaService,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<CrearEntradasSalidasComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.tipocarnet = false;
  }

  ngOnInit() {
    this.initListarFormGroup();
  }

  ngAfterViewInit() {
    this.barecodeScanner.start();
  }

  onValueChanges(result){
    this.playAudio();
    this.barecodeScanner.stop();
    this.barcodeValue = result.codeResult.code;
    if (this.barcodeValue != null ) {
      this.buscarPersona(null, this.barcodeValue);
    }
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  cambiarEstado(e) {
    this.tipocarnet = e.checked;
  }

  levantarEscaner(){
    console.log(this.barecodeScanner);
    this.barecodeScanner.start();
    this.barcodeValue = null;
    this.nuevaPersona = 'SIN_BUSQUEDA';
    this.visita = 'NO_VISITA';
  }

  playAudio(){
    const audio = new Audio();
    audio.src = '../../../../../assets/sounds/beep-07.wav';
    audio.load();
    audio.play();
  }

  initListarFormGroup(){

    this.personaFG = new FormGroup({
      'nombre':            new FormControl('', [Validators.required]),
      'apellidos':            new FormControl(''),
      'numero':     new FormControl('', [Validators.required,
        (control: AbstractControl) => Validators.minLength(8)(control),
        (control: AbstractControl) => Validators.maxLength(12)(control)]),
      'tipodoc_id':     new FormControl('', [Validators.required]),
      'tipopersona_id':  new FormControl('', [Validators.required]),
    });

     this.entradaFG = new FormGroup({
       'motivo':  new FormControl('', [Validators.required]),
       'created_at':            new FormControl('')
    });

  }

  buscarPersona(tipo, nrodni){
    this.personaService.listarPersonas(null, tipo, nrodni, null, {page: 1, limit: 10, sortDirection: null, sort: null}).subscribe(
      data => {
        if (data.data.length !== 0) {
          this.personaBuscada =  data.data[0];
          if (this.personaBuscada.tipo_persona.dato == 'VISITANTE') {
            this.visita = 'VISITANTE';
          } else {
            this.visita = 'NO_VISITA';
          }
          this.nuevaPersona = 'PERSONA_ENCONTRADA';
          this.barecodeScanner.stop();
          console.log(this.personaBuscada);
        } else  {
          this.personaBuscada = null;
          this.nuevaPersona = 'PERSONA_NO_ENCONTRADA';
          console.log(this.personaBuscada);
        }
      }
    );
  }

  cerrar() {
    this.dialogRef.close();
  }

  private onEnter(event: any)
  {
    if (event.keyCode == 13)
    {
      this.buscarPersona(null, this.barcodeValue);
    }
  }

  crearpersona (){

    const persona: IPersona = new class implements IPersona {
      apellidos: string;
      id: number;
      nombre: string;
      numero: string;
      tipo_doc: ICatalogo;
      tipo_persona: ICatalogo;
      tipo_idcarrera: Array<any>;
      tipodoc_id: string;
      tipopersona_id: string;
      sunedu: string;
    };

    const entrada_salida = {'entrada': new Date(), 'detalle': ''};

    if (!this.personaBuscada) {
      persona.apellidos = this.personaFG.get('apellidos').value;
      persona.nombre =  this.personaFG.get('nombre').value;
      persona.tipodoc_id = this.personaFG.get('tipodoc_id').value;
      persona.tipopersona_id = 4;
      persona.numero = this.personaFG.get('numero').value;
      persona['entrada_salida'] = { 'entrada': new Date(), 'detalle': this.entradaFG.get('motivo').value };
      persona.tipo_idcarrera = [];
      this.personaService.crearpersona(persona).subscribe(
        data => {
          this.openAlertdialogs('alert-succesful', 'Se registro la visita', '¡Éxito!', null, 'success');
          this.levantarEscaner();
        },
        error => {
          this.openAlertdialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
        }
      );

    } else {
      entrada_salida.detalle = this.entradaFG.get('motivo').value;

        this.personaService.entradaPersona(this.personaBuscada.id, entrada_salida).subscribe(
          data => {
            this.openAlertdialogs('alert-succesful', 'Se registro la visita', '¡Éxito!', null, 'success');
            this.levantarEscaner();
          },
          error => {
            this.openAlertdialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
          }
        );
      }


    // }
  }


}
