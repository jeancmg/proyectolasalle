import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarPersonasComponent } from './listar-personas/listar-personas.component';
import { CrearPersonaComponent } from './crear-persona/crear-persona.component';
import { DetallePersonaComponent } from './detalle-persona/detalle-persona.component';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '../../../../@fuse/shared.module';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCardModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatRippleModule, MatSelectModule,
  MatSidenavModule, MatSlideToggleModule, MatSortModule,
  MatStepperModule, MatTableModule, MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PaginatorModule} from '../../../general/componentes/paginator/paginator.module';
import {LoginModule} from '../../../security/login/login.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FuseSharedModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatSidenavModule,
    MatMenuModule,
    MatRippleModule,
    MatStepperModule,
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCardModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    PaginatorModule,
    MatIconModule,
    MatDialogModule,
    LoginModule
  ],
  declarations: [ListarPersonasComponent, CrearPersonaComponent, DetallePersonaComponent],
  entryComponents: [DetallePersonaComponent, CrearPersonaComponent]
})
export class PersonaModule { }
