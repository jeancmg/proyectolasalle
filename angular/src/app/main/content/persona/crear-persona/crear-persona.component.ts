import {Component, Inject, OnInit} from '@angular/core';
import {PersonaService} from '../../../../api/persona.service';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Tipodoc, Tipoper} from '../../entradas-salidas/crear-entradas-salidas/crear-entradas-salidas.component';
import {ICarrera, ICatalogo, IPersona, IPersonacarrera} from '../../../../api/Model';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

export interface Tipodoc {
  value: string;
  viewValue: string;
}

export interface Tipoper {
  value: string;
  viewValue: string;
}

export interface Tipocar {
  value: number;
  viewValue: string;
}


@Component({
  selector: 'fuse-app-app-crear-persona',
  templateUrl: './crear-persona.component.html',
  styleUrls: ['./crear-persona.component.scss']
})
export class CrearPersonaComponent implements OnInit {

  tipodoc: Tipodoc[] = [
    {value: '1', viewValue: 'Documento de Identidad'},
    {value: '2', viewValue: 'Carnet de Extranjeria'},
    {value: '3', viewValue: 'Pasaporte'}
  ];
  tipoper: Tipoper[] = [
    {value: '5', viewValue: 'Estudiante'},
    {value: '6', viewValue: 'Profesor'},
    {value: '8', viewValue: 'Administrativo'},
    {value: '9', viewValue: 'Limpieza'},
    {value: '10', viewValue: 'Seguridad'},
    {value: '11', viewValue: 'Egresado'},
  ];
  tipocar: Tipocar[] = [
    {value: 1, viewValue: 'Administraciòn'},
    {value: 2, viewValue: 'Derecho'},
    {value: 3, viewValue: 'Ing de Sotfware'}
  ];
  public personafromgroup: FormGroup;
  dialogRefAlert: any;
  condicion = 'DEFAULT';
  constructor(private personaService: PersonaService ,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<CrearPersonaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.initListarFormGroup();
  }

  initListarFormGroup(){
    this.personafromgroup = new FormGroup({
      'nombre':            new FormControl('', [Validators.required]),
      'apellidos':            new FormControl(''),
      'numero':     new FormControl('', [Validators.required,
        (control: AbstractControl) => Validators.minLength(8)(control),
        (control: AbstractControl) => Validators.maxLength(12)(control)]),
      'tipodoc_id':     new FormControl('', [Validators.required]),
      'tipopersona_id':  new FormControl('', [Validators.required]),
      'tipocarrera_id':  new FormControl(''),
      'sunedu':  new FormControl(''),
    });
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  condicionales(e) {
    console.log(e);
    switch (e) {
      case '5':
        this.condicion = 'ALUMNO';
        // CONDICIONES PARA ALUMNO
        break;
      case '6':
        this.condicion = 'PROFESOR';
        // CONDICIONES PARA PROFESOR
        break;
      case ('11' || 11):
        this.condicion = 'ALUMNO';
        // CONDICIONES PARA ALUMNO
        break;
      default:
        this.condicion = 'DEFAULT';
        // CONDICIONES PARA CUALQUIER PERSONA
        break;
    }
    console.log(this.condicion);
  }

  cerrar() {
    this.dialogRef.close();
  }

  crearpersona (){

    const persona: IPersona = new class implements IPersona {
      apellidos: string;
      id: number;
      nombre: string;
      numero: string;
      tipo_doc: ICatalogo;
      tipo_persona: ICatalogo;
      tipo_idcarrera: any[];
      tipodoc_id: string;
      tipopersona_id: string;
      sunedu: string;
    };
    persona.tipo_idcarrera = [];
    persona.apellidos = this.personafromgroup.get('apellidos').value;
    persona.nombre =  this.personafromgroup.get('nombre').value;
    persona.tipodoc_id = this.personafromgroup.get('tipodoc_id').value;
    persona.tipopersona_id = this.personafromgroup.get('tipopersona_id').value;
    persona.numero = this.personafromgroup.get('numero').value;
    persona.sunedu = this.personafromgroup.get('sunedu').value;
    const carreras: any[] =  this.personafromgroup.get('tipocarrera_id').value;
    console.log(this.personafromgroup.get('tipocarrera_id').value);
    for (let i = 0 ; i <  carreras.length; i++ ) {
      console.log(i);
      let carrera: any;
      carrera = {'id' : null , 'id_persona': null , 'id_carrera': carreras[i].value};
      persona.tipo_idcarrera.push(carrera);
    }
    console.log(persona);
    this.personaService.crearpersona(persona).subscribe(
      data => {
        this.cerrar();
        console.log(data);
        this.openAlertdialogs('alert-succesful', 'La persona fue creada con  éxito', '¡Éxito!', null, 'success');
      },
      error => {
        console.log(error);
        this.openAlertdialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
      }
    );


  }

}
