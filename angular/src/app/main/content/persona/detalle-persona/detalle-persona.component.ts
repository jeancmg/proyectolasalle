import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {CrearPersonaComponent, Tipocar, Tipodoc, Tipoper} from '../crear-persona/crear-persona.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {PersonaService} from '../../../../api/persona.service';
import {ICatalogo, IPersona} from '../../../../api/Model';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-app-detalle-persona',
  templateUrl: './detalle-persona.component.html',
  styleUrls: ['./detalle-persona.component.scss']
})
export class DetallePersonaComponent implements OnInit {

  tipodoc: Tipodoc[] = [
    {value: '1', viewValue: 'Documento de Identidad'},
    {value: '2', viewValue: 'Carnet de Extranjeria'},
    {value: '3', viewValue: 'Pasaporte'}
  ];
  tipoper: Tipoper[] = [
    {value: '4', viewValue: 'Visitante'},
    {value: '5', viewValue: 'Estudiante'},
    {value: '6', viewValue: 'Profesor'},
    {value: '8', viewValue: 'Administrativo'},
    {value: '9', viewValue: 'Limpieza'},
    {value: '10', viewValue: 'Seguridad'},
    {value: '11', viewValue: 'Egresado'},
  ];
  tipocar: Tipocar[] = [
    {value: 1, viewValue: 'Administraciòn'},
    {value: 2, viewValue: 'Derecho'},
    {value: 3, viewValue: 'Ing de Sotfware'}
  ];

  public personafromgroup: FormGroup;
  public personaEditar: IPersona;
  dialogRefAlert: any;
  flagPersonaID: '';
  condicion = 'DEFAULT';
  constructor(private personaService: PersonaService,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<DetallePersonaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.personaEditar = data;
    console.log(this.personaEditar);
  }

  ngOnInit() {
    this.initListarFormGroup();
    this.cargarData();
  }

  initListarFormGroup(){
    this.personafromgroup = new FormGroup({
      'nombre':            new FormControl('', [Validators.required]),
      'apellidos':            new FormControl(''),
      'numero':     new FormControl('', [Validators.required,
        (control: AbstractControl) => Validators.minLength(8)(control),
        (control: AbstractControl) => Validators.maxLength(12)(control)]),
      'tipodoc_id':     new FormControl('', [Validators.required]),
      'tipopersona_id':  new FormControl('', [Validators.required]),
      'tipocarrera_id':  new FormControl('', [Validators.required]),
      'sunedu':  new FormControl('', [Validators.required]),
    });
  }

  cerrar() {
    this.dialogRef.close();
  }

  cargarData() {
    const selectcarreras: any[] = [];
    this.personafromgroup.controls['nombre'].setValue(this.personaEditar.nombre);
    this.personafromgroup.controls['apellidos'].setValue(this.personaEditar.apellidos);
    this.personafromgroup.controls['numero'].setValue(this.personaEditar.numero);
    this.personafromgroup.controls['tipodoc_id'].setValue(this.personaEditar.tipodoc_id);
    this.personafromgroup.controls['tipopersona_id'].setValue(this.personaEditar.tipopersona_id);
    this.condicionales(this.personaEditar.tipopersona_id);
    this.personafromgroup.controls['sunedu'].setValue(this.personaEditar.sunedu);
    this.flagPersonaID = this.personaEditar.tipopersona_id;
    if ( this.personaEditar.tipo_idcarrera.length !== 0 ){
      for (let i = 0 ; i <  this.personaEditar.tipo_idcarrera.length; i++ ) {
        console.log(i);
        let carrera: any;
        carrera = {'value' : this.personaEditar.tipo_idcarrera[i].tipo_percarrera.id , 'viewValue': this.personaEditar.tipo_idcarrera[i].tipo_percarrera.nombre};
        selectcarreras.push(carrera);
      }
      console.log(selectcarreras);
    }
    this.personafromgroup.controls['tipocarrera_id'].setValue(selectcarreras);
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  condicionales(e) {
    console.log(e);
    switch (e) {
      case ('5' || 5):
        this.condicion = 'ALUMNO';
        // CONDICIONES PARA ALUMNO
        break;
      case ('6' || 6):
        this.condicion = 'PROFESOR';
        // CONDICIONES PARA PROFESOR
        break;
      case ('11' || 11):
        this.condicion = 'ALUMNO';
        // CONDICIONES PARA ALUMNO
        break;
      default:
        this.condicion = 'DEFAULT';
        // CONDICIONES PARA CUALQUIER PERSONA
        break;
    }
    console.log(this.condicion);
  }

  compareObj(val1: any, val2: any){
    if (!val1 || !val2 || !val1.value || !val2.value) {
      return false;
    }
    return val1.value == val2.value;
  }

  actualizar() {
    const personaEditada: IPersona = new class implements IPersona {
      apellidos: string;
      id: number;
      nombre: string;
      numero: string;
      sunedu: string;
      tipo_doc: ICatalogo;
      tipo_idcarrera: any[];
      tipo_persona: ICatalogo;
      tipodoc_id: string;
      tipopersona_id: string;
    };
   personaEditada.tipo_idcarrera = [];
   personaEditada.nombre = this.personafromgroup.controls['nombre'].value;
   personaEditada.apellidos = this.personafromgroup.controls['apellidos'].value;
   personaEditada.numero = this.personafromgroup.controls['numero'].value;
   personaEditada.tipodoc_id = this.personafromgroup.controls['tipodoc_id'].value;
   personaEditada.tipopersona_id = this.personafromgroup.get('tipopersona_id').value;
   if (this.condicion == 'DEFAULT') {
     personaEditada.tipo_idcarrera = [];
   } else {
     const carreras: any[] =  this.personafromgroup.get('tipocarrera_id').value;
     for (let i = 0 ; i <  carreras.length; i++ ) {
       console.log(i);
       let carrera: any;
       carrera = {'id' : null , 'id_persona': null , 'id_carrera': carreras[i].value};
       personaEditada.tipo_idcarrera.push(carrera);
     }
   }

    console.log(personaEditada);

   this.personaService.actualizarpersona(personaEditada, this.personaEditar.id).subscribe(
     data => {
       console.log(data);
       this.openAlertdialogs('alert-succesful', 'La persona fue editada con  éxito', '¡Éxito!', null, 'success');
     },
     error => {
       console.log(error);
       this.openAlertdialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
     }
   );

  }



}
