import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListaUsersComponent} from './lista-users/lista-users.component';
import {FuseSharedModule} from '../../../../@fuse/shared.module';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PaginatorModule} from '../../../general/componentes/paginator/paginator.module';
import {LoginModule} from '../../../security/login/login.module';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';
import {RouterModule} from '@angular/router';
import {DetalleUsersComponent} from './detalle-users/detalle-users.component';
import {NuevoUserComponent} from './nuevo-user/nuevo-user.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FuseSharedModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatSidenavModule,
    MatMenuModule,
    MatRippleModule,
    MatStepperModule,
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCardModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    PaginatorModule,
    LoginModule
  ],
  declarations: [ListaUsersComponent, DetalleUsersComponent, NuevoUserComponent],
  entryComponents: [
    AlertDialogComponent,
    DetalleUsersComponent
  ]
})
export class SuperAdminModule {
}
