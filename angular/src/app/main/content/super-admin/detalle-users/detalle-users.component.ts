import {Component, Inject, OnInit} from '@angular/core';
import {IRole, IUser} from '../../../../api/Model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {UserService} from '../../../../api/user/user.service';
import {RoleService} from '../../../../api/role.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-detalle-users',
  templateUrl: './detalle-users.component.html',
  styleUrls: ['./detalle-users.component.scss']
})
export class DetalleUsersComponent implements OnInit {

  idUsuario: number;
  usuario: IUser = {
    password: null,
    name: null,
    lastname: null,
    email: null,
    role_id: null,
    deleted_at: null,
    updated_at: null,
    created_at: null,
    id: null
  };
  spinnerloader: boolean;
  public usuarioFormGroup: FormGroup;
  public SubData: Subscription;
  public loagingAutocomplete = false;
  public editEnabled = false;
  dialogRefAlert: any;
  public role: IRole;
  public roleArr: any = [];
  public sucursalArr: any = [];
  max_Length = 8;
  min_Length = 8;
  type = true;

  constructor(private usuarioService: UserService,
              private roleService: RoleService,
              public dialogRef: MatDialogRef<DetalleUsersComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              public dialog: MatDialog) {
    this.idUsuario = data.id;
    this.spinnerloader = false;
  }

  ngOnInit() {
    this.initCombos();
    this.initUsuarioFormGroup();
    this.initservice();
  }

  initUsuarioFormGroup() {
    this.usuarioFormGroup = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'lastname': new FormControl('', [Validators.required]),
      'password': new FormControl(''),
      'role_id': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.email])
    });
    this.usuarioFormGroup.disable();
  }

  initCombos() {
    this.spinnerloader = true;
    this.roleService.roles().subscribe(
      data => {
        console.log('permisos', data);
        this.role = data;
        const x: any = data;
        this.roleArr = x.data;
      },
      error => {
        this.spinnerloader = false;
        this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
      }
    );
  }

  initservice() {
    this.spinnerloader = true;
    this.usuarioService.usuario(this.idUsuario).subscribe(
      data => {
        this.spinnerloader = false;
        this.usuario = data;
        this.usuarioFormGroup.controls['name'].setValue(this.usuario.name);
        this.usuarioFormGroup.controls['lastname'].setValue(this.usuario.lastname);
        this.usuarioFormGroup.controls['email'].setValue(this.usuario.email);
        this.usuarioFormGroup.controls['role_id'].setValue(this.usuario['permissions'][0].role_id);
        console.log(data);
      },
      error => {
        this.spinnerloader = false;
        this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'cancel');
      }
    );
  }

  dialogs(responseService: string, message: string, header: string, error: any[], icon ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  edit() {
    if (this.editEnabled === false) {
      this.editEnabled = true;
      this.usuarioFormGroup.enable();
    } else {
      this.spinnerloader = true;
      const usuario: IUser = this.usuario;
      usuario.name = this.usuarioFormGroup.get('name').value;
      usuario.lastname = this.usuarioFormGroup.get('lastname').value;
      usuario.email = this.usuarioFormGroup.get('email').value;
      usuario.password = this.usuarioFormGroup.get('password').value;
      usuario.role_id = this.usuarioFormGroup.get('role_id').value;

      console.log(usuario);

      this.usuarioService.edit(usuario, this.idUsuario).subscribe(
        data => {
          this.spinnerloader = false;
          this.dialogRef.close(1);
          this.dialogs('alert-succesful', 'El usuario fue actualizado con  éxito', '¡Éxito!', null, 'success');
        },
        error => {
          this.spinnerloader = false;
          this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
        }
      );

    }
  }

}
