import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MatSidenavModule} from '@angular/material';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseNavigationModule, FuseSidebarModule} from '@fuse/components';
import {FuseContentModule} from 'app/main/content/content.module';
import {FuseNavbarModule} from 'app/main/navbar/navbar.module';
import {FuseToolbarModule} from 'app/main/toolbar/toolbar.module';
import {FuseMainComponent} from './main.component';
import {AuthGuard} from '../security/login/guard/auth.guard';
import {ListaUsersComponent} from './content/super-admin/lista-users/lista-users.component';
import {SuperAdminModule} from './content/super-admin/super-admin.module';
import {Role} from '../security/login/models/roles';
import {NuevoUserComponent} from './content/super-admin/nuevo-user/nuevo-user.component';
import {ListarPersonasComponent} from './content/persona/listar-personas/listar-personas.component';
import {CrearPersonaComponent} from './content/persona/crear-persona/crear-persona.component';
import {PersonaModule} from './content/persona/persona.module';
import {EntradasSalidasModule} from './content/entradas-salidas/entradas-salidas.module';
import {ListarEntradasSalidasComponent} from './content/entradas-salidas/listar-entradas-salidas/listar-entradas-salidas.component';
import {CrearEntradasSalidasComponent} from './content/entradas-salidas/crear-entradas-salidas/crear-entradas-salidas.component';

const appRoutes: Routes = [
  {
    path: 'app',
    component: FuseMainComponent,
    children : [
      {
        path: 'administracion',
        component: ListaUsersComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'administracion/nuevo',
        component: NuevoUserComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'personas',
        component: ListarPersonasComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'personas/nuevo',
        component: CrearPersonaComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'entradas-salidas',
        component: ListarEntradasSalidasComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'entradas-salidas/nuevo',
        component: CrearEntradasSalidasComponent,
        canActivate: [AuthGuard],
      },
      { path: '', redirectTo: 'personas', pathMatch: 'full' }
    ]
  },
];

@NgModule({
    declarations: [
        FuseMainComponent
    ],
    imports     : [
        // Material
        RouterModule.forChild(appRoutes),
        MatSidenavModule,
        // Modulos Nuestros
        SuperAdminModule,
        PersonaModule,
        EntradasSalidasModule,
        // Fuse
        FuseSharedModule,
        FuseNavigationModule,
        FuseSidebarModule,
        FuseContentModule,
        FuseNavbarModule,
        FuseToolbarModule,

    ],
    exports     : [
        FuseMainComponent
    ]
})
export class FuseMainModule
{
}
