import {Component} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseConfigService} from '@fuse/services/config.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {navigation} from 'app/navigation/navigation';
import {AuthenticationService} from '../../security/login/services/authentication.service';

@Component({
    selector   : 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class FuseToolbarComponent
{
    showLoadingBar: boolean;
    horizontalNav: boolean;
    noNav: boolean;
    navigation: any;
    host: string;
      username = 'Nuevo Usuario';
    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private sidebarService: FuseSidebarService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private authenticationService: AuthenticationService
    )
    {
        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showLoadingBar = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showLoadingBar = false;
                }
            });

        this.fuseConfig.onConfigChanged.subscribe((settings) => {
            this.horizontalNav = settings.layout.navigation === 'top';
            this.noNav = settings.layout.navigation === 'none';
        });

        this.navigation = navigation;
        // this.getUsuario();
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }


    logout(){
      this.authenticationService.logout(false);
      this.router.navigate(['/login']);
    }

    isLogin(): boolean{
      return this.authenticationService.isLogin();
    }

      getUsuario(): string{
      const user = this.authenticationService.getUser();
      this.username = user.user.name;
      return user.user.name;
    }

}
